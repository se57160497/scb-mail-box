import {Component, OnInit} from '@angular/core';
import emailContent from '../../../../../shared/interfaces/mail/mail-content.interface';
import {InboxService} from '../../../../../shared/services/mail/inbox.service';

// // tslint:disable-next-line:class-name
// interface emailContent {
//   id?: number;
//   sender: fromSender;
//   subject: string;
//   body: string;
//   checked?: boolean;
//   loading?: boolean;
// }
//
// // tslint:disable-next-line:class-name
// interface fromSender {
//   name: string;
//   email: string;
// }
//
// // tslint:disable-next-line:class-name
// interface fromSender {
//   name: string;
//   email: string;
// }
//
// const mockData: emailContent[] = [
//   {
//     sender: {
//       name: 'Now TV',
//       email: 'nowtv@test.com'
//     },
//     subject: 'Grab another Pass, you need to be watching this...',
//     // tslint:disable-next-line:max-line-length
//     body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.'
//   },
//   {
//     sender: {
//       name: 'Investopedia Terms',
//       email: 'investopedia@test.com'
//     },
//     subject: 'What is \'Fibonanci Retracement\'?',
//     // tslint:disable-next-line:max-line-length
//     body: 'Fibonacci retracement is a term used in technical analysis that refers to areas of support (price stops going lower) or resistance (price stops going higher).'
//   },
//   {
//     sender: {
//       name: 'ASICS Greater Manchester Marathon ',
//       email: 'events@human-race.co.uk'
//     },
//     subject: 'Your chance to take on the marathon',
//     // tslint:disable-next-line:max-line-length
//     body: 'Do you feel inspired to take on one of Europe\'s most highly regarded and popular marathons?'
//   }
// ];


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  initLoading = true; // bug
  loadingMore = false;
  list: Array<emailContent> = [];

  state = {
    loadingContent: true
  };

  constructor(private inboxService: InboxService) {
  }

  loading() {
    this.state.loadingContent = true;
  }

  loaded() {
    this.state.loadingContent = false;
  }

  ngOnInit(): void {
    this.list = [];
    this.onLoadMore();
    this.initLoading = false;
  }

  onLoadMore(): void {
    this.loading();
    const responseData = this.inboxService.getInboxData();
    [...responseData].map((mock) => {
      mock.id = this.list.length;
      this.list.push({...mock});
    });
    setTimeout(() => {
      this.loaded();
    }, 2000);
  }
}
