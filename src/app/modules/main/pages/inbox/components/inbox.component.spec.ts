import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InboxComponent} from './inbox.component';
// @ts-ignore
import {InboxService} from '../../../../../shared/services/mail/inbox.service';

describe('InboxComponent', () => {
  let component: InboxComponent;
  let inboxService: InboxService;
  let fixture: ComponentFixture<InboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InboxComponent],
      providers: [InboxService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxComponent);
    component = fixture.componentInstance;
    inboxService = new InboxService();
    fixture.detectChanges();
  });

  it('[DETECT] should create', () => {
    expect(component).toBeTruthy();
  });

  it('[DETECT] should have loadMore Function', () => {
    expect(typeof component.onLoadMore).toEqual('function');
  });

  it('[DETECT] should have loading Function', () => {
    expect(typeof component.loading).toEqual('function');
  });

  it('[DETECT] should have loaded Function', () => {
    expect(typeof component.loaded).toEqual('function');
  });

  it('[LOGIC] loading function working!', () => {
    expect(typeof component.loaded).toEqual('function');
    component.loading();
    expect(component.state.loadingContent).toEqual(true);
  });

  it('[LOGIC] loaded function working!', () => {
    expect(typeof component.loaded).toEqual('function');
    component.loaded();
    expect(component.state.loadingContent).toEqual(false);
  });

  it('[LOGIC] onloadMore function working!', () => {
    expect(typeof component.onLoadMore).toEqual('function');
    const lengthBefore = component.list.length;
    component.onLoadMore();
    expect(component.list.length === lengthBefore).toBeFalse();
  });

  it('[SERVICE] have InboxData Function working!', () => {
    expect(typeof inboxService.getInboxData).toEqual('function');
  });
});
