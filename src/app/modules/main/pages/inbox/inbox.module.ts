import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InboxRoutingModule } from './inbox-routing.module';
import { InboxComponent } from './components/inbox.component';
import {
  NzAvatarModule,
  NzButtonModule,
  NzDropDownModule,
  NzGridModule,
  NzIconModule,
  NzListModule,
  NzSkeletonModule,
  NzTableModule, NzTypographyModule
} from 'ng-zorro-antd';


@NgModule({
  declarations: [InboxComponent],
  imports: [
    CommonModule,
    InboxRoutingModule,
    NzTableModule,
    NzListModule,
    NzSkeletonModule,
    NzButtonModule,
    NzGridModule,
    NzDropDownModule,
    NzIconModule,
    NzAvatarModule,
    NzTypographyModule
  ]
})
export class InboxModule { }
