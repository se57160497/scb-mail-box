import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './core/main.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {path: 'inbox', loadChildren: () => import('./pages/inbox/inbox.module').then(m => m.InboxModule)},
      {
        path: '**',
        redirectTo: 'inbox'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
