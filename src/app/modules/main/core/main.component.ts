import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {
  isCollapsed = false;
  listBreadcrumb = [];

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.listBreadcrumb = this.router.url.split('/').filter(f => f.trim().length > 0);
  }

}
