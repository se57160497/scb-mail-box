import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainRoutingModule} from './main-routing.module';
import {MainComponent} from './core/main.component';
import {NzBreadCrumbModule, NzIconModule, NzLayoutModule, NzMenuModule} from 'ng-zorro-antd';
import {BreadcrumbComponent} from '../../shared/components/breadcrumb/breadcrumb.component';


@NgModule({
  declarations: [MainComponent, BreadcrumbComponent],
  imports: [
    CommonModule,
    MainRoutingModule,
    NzLayoutModule,
    NzMenuModule,
    NzIconModule,
    NzBreadCrumbModule
  ]
})
export class MainModule {
}
