import {Injectable} from '@angular/core';
import emailContent from '../../interfaces/mail/mail-content.interface';

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  getInboxData() {
    const emailContents: emailContent[] = [
      {
        sender: {
          name: 'Now TV',
          email: 'nowtv@test.com'
        },
        subject: 'Grab another Pass, you need to be watching this...',
        // tslint:disable-next-line:max-line-length
        body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.'
      },
      {
        sender: {
          name: 'Investopedia Terms',
          email: 'investopedia@test.com'
        },
        subject: 'What is \'Fibonanci Retracement\'?',
        // tslint:disable-next-line:max-line-length
        body: 'Fibonacci retracement is a term used in technical analysis that refers to areas of support (price stops going lower) or resistance (price stops going higher).'
      },
      {
        sender: {
          name: 'ASICS Greater Manchester Marathon ',
          email: 'events@human-race.co.uk'
        },
        subject: 'Your chance to take on the marathon',
        // tslint:disable-next-line:max-line-length
        body: 'Do you feel inspired to take on one of Europe\'s most highly regarded and popular marathons?'
      }
    ];
    return emailContents;
  }
}
