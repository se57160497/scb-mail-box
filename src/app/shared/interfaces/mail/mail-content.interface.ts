// tslint:disable-next-line:class-name
export default interface emailContent {
  id?: number;
  sender: fromSender;
  subject: string;
  body: string;
  checked?: boolean;
  loading?: boolean;
}

// tslint:disable-next-line:class-name
export interface fromSender {
  name: string;
  email: string;
}

// tslint:disable-next-line:class-name
export interface fromSender {
  name: string;
  email: string;
}
