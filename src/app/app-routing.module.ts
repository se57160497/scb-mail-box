import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


const routes: Routes = [
  // {
  //   path: 'login',
  //   loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
  // },
  {
    path: 'mail',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule),
  },
  {
    path: '**',
    redirectTo: 'mail',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
